import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import logo from "../assets/logo.png";

const style = {
    fontSize: "3rem" 
}

const Header = () => (
    <Navbar bg="primary" variant="dark" expand="lg" >
    <div className="container">
      <Navbar.Brand href="#" style={style} >
        <img
          src={logo}
          width="80"
          height="80"
          className=" mr-2 d-inline-flex"
          alt="React Bootstrap logo"
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"  />
      <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
        <Nav className="ml-auto">
          <Nav.Link href="#">Profil</Nav.Link>
          <Nav.Link href="#">Déconnexion</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </div>
  </Navbar>

);

export default Header;
