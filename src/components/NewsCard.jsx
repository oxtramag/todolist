import React from "react";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import { useState } from "react";

const style = {
  height: "6.25rem",
};

const NewsCard = () => {

  const [stateInput, setStateInput] = useState();

  const linkdInput = e => {
    console.log(e);
  }



  return (
    <form>
      {/* ---------Titre---------- */}
      <FloatingLabel controlId="floatingTitle" label="Titre" className="mb-3">
        <Form.Control onInput={e => linkdInput(e.target.value)} type="text" placeholder="titre" />
      </FloatingLabel>
      {/* ---------Pseudo---------- */}
      <FloatingLabel controlId="floatingName" label="Pseudo" className="mb-3">
        <Form.Control type="text" placeholder="Name" />
      </FloatingLabel>
      {/* ---------Email---------- */}
      <FloatingLabel
        controlId="floatingInput"
        label="Adresse Email"
        className="mb-3"
      >
        <Form.Control type="email" placeholder="name@example.com" />
      </FloatingLabel>
      {/* ---------Description---------- */}
      <FloatingLabel
        controlId="floatingDescription"
        label="Description"
        className="mb-3"
      >
        <Form.Control
          as="textarea"
          placeholder="Ajoute une desciption ici"
          style={style}
        />
      </FloatingLabel>
      {/* ---------Categories---------- */}
      <Form.Select aria-label="selectCategories" className="mb-3">
        <option>choisi une catégorie</option>
        <option value="cat1">Travail</option>
        <option value="cat2">Loisir</option>
        <option value="cat3">Fun</option>
      </Form.Select>
      {/* ---------Prioritée---------- */}
      <Form.Select aria-label="selectPrio" className="mb-3">
        <option value="prio1">Prioritée Moyenne</option>
        <option value="prio2">Prioritée Basse</option>
        <option value="prio3">Prioritée Haute</option>
      </Form.Select>
      {/* ---------Statut---------- */}
      <Form.Select aria-label="selectStatut" className="mb-3">
                <option value="statut1">En attente</option>
                <option value="statut2">En cours</option>
                <option value="statut3">Fini</option>
              </Form.Select>
      {/* ---------Select Date Max---------- */}
      <Form.Group controlId="dateMax">
        <Form.Label className="">Choisi une date maximun :</Form.Label>
        <Form.Control type="date" name="dateMax" placeholder="Date maximum" />
      </Form.Group>
    </form>
  );
};

export default NewsCard;
