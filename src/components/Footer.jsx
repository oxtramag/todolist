import React from "react";

const style = {
    container: {
        backgroundColor: "rgba(0, 0, 0, 0.025)",
    }
};

const Footer = () => (
  <footer className="text-center text-lg-start bg-light text-muted fixed-bottom">
    <div className="text-center p-4" style={style.container}>
      © 2023 Copyright:
      <a className="text-reset fw-bold mx-2" href="#">
        by Olivia Gautheron
      </a>
    </div>
  </footer>
);

export default Footer;
