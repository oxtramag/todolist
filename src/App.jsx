import { useState } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import NewsCard from "./components/NewsCard";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

const style = {
  col: {
    backgroundColor: "rgba(0, 0, 0, 0.025)",
    height: "5rem",
    borderTop: "solid 0.5rem #0d6efd",
    borderBottom: "solid 0.2rem #0d6efd",
    borderLeft: "solid 0.1rem #0d6efd",
    borderRight: "solid 0.1rem #0d6efd",
  },
};



function App() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  const [dataArr, setDataArr] = useState([
    {
      id: "01",
      dateD: "17/04/2023",
      name: "Faire une ToDoList",
      describe: `Prérequis : l'utilisation d'une base de donnée est obligatoire....`,
      cat: "Travail",
      dateU: "il y a 2mn",
      email: "John Doe JohnDoe@gmail.com",
      prio: "Priorité haute",
      dateF: "Fin le : 19/04/2023",
      statut: "En cours",
    },

    {
      id: "02",
      dateD: "17/04/2023",
      name: "Jouer teemo sur LoL",
      describe: `Faire rager le jungler avec des champignons toxique...`,
      cat: "Loisir",
      dateU: "il y a 2jour",
      email: "John Doe JohnDoe@gmail.com",
      prio: "Priorité basse",
      dateF: "Fin le : 22/08/2023",
      statut: "En cours",
    },

    {
      id: "03",
      dateD: "18/04/2023",
      name: "Recensement des blagues belges",
      describe: `1-Comment rendre un Belge fou ? L’enfermer dans une pièce ronde et
      lui dire qu’il y a une frite dans un coin...`,
      cat: "Fun",
      dateU: "il y a 13h",
      email: "John Doe JohnDoe@gmail.com",
      prio: "Priorité moyenne",
      dateF: "Fin le : 29/06/2023",
      statut: "En attente",
    },
  ])

  const deleteRow = id => {
    const filteredState = dataArr.filter(item => {
      return item.id !== id;
    })
    setDataArr(filteredState)
  }
  

  return (
    <div className="App">
      <Header />
      <h1 className="text-center text-primary my-4">ToDoList</h1>

      <Modal size="l" show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Ajouter une tâche</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <NewsCard />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
          <Button type="submit" variant="primary" onClick={handleClose}>
            Ajouter
          </Button>
        </Modal.Footer>
      </Modal>

      <div>
        <Container className="mt-3" fluid="xxl">
          <div className="d-flex p-3 justify-content-end">
            <Button variant="outline-primary" onClick={handleShow}>
              + ajouter une tâche
            </Button>
          </div>

          {dataArr.map((item, i) => {
            return (
              <Row
                className="mb-3 d-flex align-items-center"
                key={i}
                data={item}
              >
                <Col style={style.col} className="d-flex align-items-center">
                  {item.dateD}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.name}
                </Col>
                <Col
                  xs={3}
                  style={style.col}
                  className="d-flex align-items-center"
                >
                  {item.describe}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.cat}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.dateU}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.email}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.prio}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.dateF}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  {item.statut}
                </Col>
                <Col style={style.col} className="d-flex align-items-center">
                  <Button variant="outline-primary" className="m-1">
                    Edit
                  </Button>
                  <Button variant="btn btn-danger" onClick={() => deleteRow(item.id) }>X</Button>
                </Col>
              </Row>
            );
          })}
        </Container>
      </div>

      <Footer />
    </div>
  );
}

export default App;
